package satellite;

import java.io.FileNotFoundException;
import java.util.Scanner;

import satellite.telemetry.TelemetryReader;

public class App{
    public static void main( String[] args ) throws FileNotFoundException {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a file to process");
        String filename = scanner.nextLine();

        TelemetryReader tr = new TelemetryReader(filename);
        tr.ingestFile();
    }
}
