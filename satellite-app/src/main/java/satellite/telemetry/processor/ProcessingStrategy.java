package satellite.telemetry.processor;

import satellite.telemetry.data.TelemetryReading;

public interface ProcessingStrategy {

    public boolean errorDetected(TelemetryReading tr);
}
