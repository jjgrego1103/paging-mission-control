package satellite.telemetry.processor;

import satellite.telemetry.data.TelemetryReading;

public class BatteryStrategy implements ProcessingStrategy{
    @Override
    public boolean errorDetected(TelemetryReading tr) {
        //If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.
        return (tr.getRawValue() < tr.getRedLowLimit());
    }
}
