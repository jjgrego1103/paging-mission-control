package satellite.telemetry.processor;

import satellite.telemetry.data.TelemetryReading;

public class ThermostatStrategy implements ProcessingStrategy{

    @Override
    public boolean errorDetected(TelemetryReading tr) {
       // If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.
        return (tr.getRawValue() > tr.getRedHighLimit() );
    }
}
