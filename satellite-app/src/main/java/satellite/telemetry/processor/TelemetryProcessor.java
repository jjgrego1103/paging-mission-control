package satellite.telemetry.processor;

import java.util.*;
import java.util.stream.Collectors;

import satellite.telemetry.communications.AlertManager;
import satellite.telemetry.data.TelemetryReading;

public class TelemetryProcessor {
//    private Map<String, List<TelemetryReading>> batMap;
//    private Map<String, List<TelemetryReading>> tempMap;

    private List<TelemetryReading> errorList;

    private AlertManager am;
    private ProcessingStrategy tempStrategy;
    private ProcessingStrategy battStrategy;

    public TelemetryProcessor() {
        this.errorList = new ArrayList<>();
        this.am = new AlertManager();
        this.tempStrategy = new ThermostatStrategy();
        this.battStrategy = new BatteryStrategy();
    }

    public void processReadings(Map<String, List<TelemetryReading>> readings) {
        for (String key : readings.keySet()) {
        	//System.out.println("checking key:  " + key + "\n");
            List<TelemetryReading> listOfReadings = readings.get(key);
		if(errorList.size() > 0){
			errorList.clear();
		}
			for(TelemetryReading byType : listOfReadings){
				//System.out.println("++++" + byType);
				switch (key){
					case "TSTAT":
							checkTemperature(byType);
						break;
					case "BATT":
							checkBattery(byType);
						break;
					default:
						System.out.println("Unsure of component type... omitting reading");
				}
			}
			computeErrorMessages();
        }

		am.printAlertMessages();
    }

    private void computeErrorMessages() {
        Map<String, List<TelemetryReading>> errorMap = convertList(errorList);
        for (String satId : errorMap.keySet()) {
            List<TelemetryReading> errorReadings = errorMap.get(satId);
            if (errorReadings.size() > 2) {
                am.generateErrorMessages(errorReadings);
            }
        }
    }

    private Map<String, List<TelemetryReading>> convertList(List<TelemetryReading> readings) {
        return readings.stream()
                .collect(Collectors.groupingBy(TelemetryReading::getSatelliteId));
    }

	private void checkBattery(TelemetryReading tr) {
		boolean alertError = battStrategy.errorDetected(tr);
		if (alertError) {
			errorList.add(tr);
		}
	}

    private void checkTemperature(TelemetryReading tr) {
        boolean alertError = tempStrategy.errorDetected(tr);
        if (alertError) {
            errorList.add(tr);
        }
    }
}