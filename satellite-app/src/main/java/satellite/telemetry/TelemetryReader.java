package satellite.telemetry;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import satellite.telemetry.data.TelemetryReading;
import satellite.telemetry.processor.TelemetryProcessor;

public class TelemetryReader {

    private String fileName;
    private TelemetryProcessor tp;

    public TelemetryReader(String fName){
        this.fileName = fName;
        this.tp = new TelemetryProcessor();
    }

    public void ingestFile(){
        File infile = new File(fileName);
        List<TelemetryReading> readings = new ArrayList<>();
        try {
            Scanner scnr = new Scanner(infile);
            while (scnr.hasNextLine()) {
                String line = scnr.nextLine();
                TelemetryReading tr = parseLineData(line);
                readings.add(tr);
            }
        } catch(FileNotFoundException fnfe){
            System.err.println("Need to input a valid file path...");
        }
        Map<String, List<TelemetryReading>> readingMap = convertList(readings);

        tp.processReadings(readingMap);
    }

    private Map<String, List<TelemetryReading>> convertList(List<TelemetryReading> readings) {
        return readings.stream()
                .collect(Collectors.groupingBy(TelemetryReading::getComponent));
    }

    private TelemetryReading parseLineData(String lineIn) {

    	TelemetryReading tr = new TelemetryReading();
		String[] dataArray = lineIn.split("\\|");
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		
		try {
			Date dateTime = formatter.parse(dataArray[0]);
			tr.setTimestamp(dateTime);
		} catch (ParseException e) {
			//assuming the right format is given to us
		}
		
		tr.setSatelliteId(dataArray[1]);
		tr.setRedHighLimit(Float.parseFloat(dataArray[2]));
		tr.setYellowHighLimit(Float.parseFloat(dataArray[3]));
		tr.setYellowLowLimit(Float.parseFloat(dataArray[4]));
		tr.setRedLowLimit(Float.parseFloat(dataArray[5]));
		tr.setRawValue(Float.parseFloat(dataArray[6]));
		tr.setComponent(dataArray[7]);
		return tr;
    }
}
