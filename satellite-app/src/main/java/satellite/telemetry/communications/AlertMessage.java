package satellite.telemetry.communications;

import satellite.telemetry.data.TelemetryReading;

import java.util.Date;

public class AlertMessage{
    private String satelliteId;
    private String severity;
    private String component;
    private Date timestamp;

    private static final String LOWBATT = "RED LOW";
    private static final String HIGHTEMP = "RED HIGH";

    public AlertMessage(TelemetryReading tr) {
        this.satelliteId = tr.getSatelliteId();
        this.component = tr.getComponent();
        this.timestamp = tr.getTimestamp();
        this.severity = (tr.getComponent().equalsIgnoreCase("TSTAT") ? HIGHTEMP : LOWBATT);
    }

    public String getSatelliteId() {
        return satelliteId;
    }

    public void setSatelliteId(String satelliteId) {
        this.satelliteId = satelliteId;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString(){
        // normally, i'd use jackson or GSON to generate json data, however, if run, no guarantee it'll be on the classpath...
        StringBuilder sb = new StringBuilder();
        sb.append("\n    {");
        sb.append("\n     satelliteId: " + this.satelliteId + ",");
        sb.append("\n     severity: "    + this.severity    + ",");
        sb.append("\n     component: "   + this.component   + ",");
        sb.append("\n     timestamp: "   + this.timestamp);
        sb.append("\n    }");

        return sb.toString();
    }
}
