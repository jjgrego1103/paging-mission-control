package satellite.telemetry.communications;


import satellite.telemetry.data.TelemetryReading;

import java.util.*;

public class MessageGenerator {


    public MessageGenerator() {     }

    private boolean checkTime(TelemetryReading tr1, TelemetryReading laterReading) {
        Date d1 = tr1.getTimestamp();
        Date d2 = laterReading.getTimestamp();

        if (laterReading.getTimestamp().getTime() - tr1.getTimestamp().getTime() < 300000) {
            return true;
        }
        return false;
    }

    public List<AlertMessage> generateErrorMessages(List<TelemetryReading> errorList) {
        // if there are < 3 entries per satellite, then there can't be 3 readings within a 5 min interval...
        List<AlertMessage> msgs = new ArrayList<>();
        TelemetryReading[] trs = errorList.toArray(new TelemetryReading[errorList.size()]);
        Arrays.sort(trs);
        LinkedList<TelemetryReading> timedErrors = new LinkedList<>();
        for (TelemetryReading tr : trs) {
            switch (timedErrors.size()) {
                case 0:
                    // as there is nothing on the list, add it
                    timedErrors.add(tr);
                    break;
                case 1:
                    if (checkTime(timedErrors.getFirst(), tr)) {
                        // so there is an entry on the list... it it's within 5 min interval add to list
                        timedErrors.addLast(tr);
                        break;
                    } else {
                        // otherwise, outside 5 min interval, clear the list & add new reading
                        timedErrors.clear();
                        timedErrors.add(tr);
                    }
                    break;
                case 2:
                    if (checkTime(timedErrors.getFirst(), tr)) {
                        // add error message to error msg List
                        msgs.add(new AlertMessage(tr));
                        // remove first entry
                        timedErrors.removeFirst();
                        // add the tr to the tail of the list in the event another error pops up...
                        timedErrors.addLast(tr);
                    } else {
                        // otherwise, outside 5 min window, clear the list & add new reading
                        if (checkTime(timedErrors.getLast(), tr)) {
                            // ok, the 2nd entry was within 5 min of this reading, so add new reading to timed errors
                            timedErrors.removeFirst();
                            timedErrors.addLast(tr);
                        } else {
                            timedErrors.clear();
                            timedErrors.addFirst(tr);
                        }
                    }
                    break;
            }

        }
        return msgs;
    }
}
