package satellite.telemetry.communications;


import satellite.telemetry.data.TelemetryReading;

import java.util.ArrayList;
import java.util.List;

public class AlertManager {

    private List<AlertMessage> messages;
    private MessageGenerator mg;


    public AlertManager() {
        this.messages = new ArrayList<>();
        this.mg = new MessageGenerator();
    }


    public void generateErrorMessages(List<TelemetryReading> tr){
       List<AlertMessage> msgs = mg.generateErrorMessages(tr);
       if(msgs != null && msgs.size() > 0){
           messages.addAll(msgs);
       }
    }

    public void printAlertMessages(){
        // normally, i'd use jackson or GSON to generate json data, however, if run, no guarantee it'll be on the classpath...
        StringBuilder sb = new StringBuilder();
        sb.append("\n[");
        for(int i = 0; i < messages.size(); i++){
            sb.append("  " + messages.get(i).toString());
            if (i != messages.size() -1){
                sb.append(",");
            }
        }
        sb.append("\n]");
        System.out.println(sb.toString());
    }
}
