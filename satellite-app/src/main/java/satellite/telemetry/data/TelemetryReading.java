package satellite.telemetry.data;

import java.util.Date;

public class TelemetryReading implements Comparable<TelemetryReading> {

    private Date timestamp;
    private String satelliteId;
    private float redHighLimit;
    private float yellowHighLimit;
    private float yellowLowLimit;
    private float redLowLimit;
    private float rawValue;
    private String component;
    
    public TelemetryReading() {      }
	
	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getSatelliteId() {
		return satelliteId;
	}

	public void setSatelliteId(String satelliteId) {
		this.satelliteId = satelliteId;
	}

	public float getRedHighLimit() {
		return redHighLimit;
	}

	public void setRedHighLimit(float redHighLimit) {
		this.redHighLimit = redHighLimit;
	}

	public float getYellowHighLimit() {
		return yellowHighLimit;
	}

	public void setYellowHighLimit(float yellowHighLimit) {
		this.yellowHighLimit = yellowHighLimit;
	}

	public float getYellowLowLimit() {
		return yellowLowLimit;
	}

	public void setYellowLowLimit(float yellowLowLimit) {
		this.yellowLowLimit = yellowLowLimit;
	}

	public float getRedLowLimit() {
		return redLowLimit;
	}

	public void setRedLowLimit(float redLowLimit) {
		this.redLowLimit = redLowLimit;
	}

	public float getRawValue() {
		return rawValue;
	}

	public void setRawValue(float rawValue) {
		this.rawValue = rawValue;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

	@Override
	public int compareTo(TelemetryReading tr) {
		return this.timestamp.compareTo(tr.getTimestamp());
	}

	@Override
	public String toString() {
		return "TelemetryReading{" +
				"timestamp=" + timestamp +
				", satelliteId='" + satelliteId + '\'' +
				", redHighLimit=" + redHighLimit +
				", yellowHighLimit=" + yellowHighLimit +
				", yellowLowLimit=" + yellowLowLimit +
				", redLowLimit=" + redLowLimit +
				", rawValue=" + rawValue +
				", component='" + component + '\'' +
				'}';
	}
}
